from dotenv import load_dotenv
from os.path import join, dirname
from flasgger import Swagger
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_marshmallow import Marshmallow

from solution_app import create_app

app = create_app()
ma = Marshmallow(app)
api = Api(app)

if __name__ == '__main__':
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)

    swagger = Swagger(app, parse=True)
    db = SQLAlchemy(app)

    from solution_app.views.views import *
    api.add_resource(AddClientView, '/add_client')
    api.add_resource(UpdateClientView, '/update_client')
    api.add_resource(DeleteClientView, '/delete_client')
    api.add_resource(AddMailingListView, '/add_mailing')
    api.add_resource(UpdateMailingListView, '/update_mailing')

    api.add_resource(DeleteMailingListView, '/delete_mailing')
    api.add_resource(TotalStatistics, '/total_statistic')
    api.add_resource(DetailedStatistics, '/detailed_statistics')
    app.run()
