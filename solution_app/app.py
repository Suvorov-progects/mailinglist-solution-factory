from flask import Flask


def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@localhost/solution'
    app.config['SWAGGER'] = {
        'title': 'Solution tasks',
    }
    return app
