from flask_restful import Resource
from flask import request
from apscheduler.schedulers.background import BackgroundScheduler

from ..utils import *


from solution_app.database.schemas import MessageSchema


sched = BackgroundScheduler(daemon=True)


class AddClientView(Resource):
    """
    добавления нового клиента в справочник со всеми его атрибутами
    """
    def post(self):
        """
        file: swagger_forms/add_client.yml
        """
        data = request.form
        valid = clientvalidate(data)
        if valid.get('data'):
            return 'не валидные данные для записи', 304
        else:
            sess.add(Client(**valid))
            sess.commit()
            return 201


class UpdateClientView(Resource):
    """
    обновления данных атрибутов клиента
    """
    def post(self):
        """
        file: swagger_forms/update_client.yml
        """
        data = request.form
        valid = clien_update_tvalidate(data)
        sess.query(Client).filter(Client.id == valid.get('id')).update(valid)
        sess.commit()
        return 201


class DeleteClientView(Resource):
    """
    удаления клиента из справочника
    """
    def post(self):
        """
        file: swagger_forms/del_client.yml
        """
        data = request.form
        sess.query(Client).filter(Client.id == data.get('id')).delete()
        sess.commit()
        return 201


class AddMailingListView(Resource):
    """
    добавления новой рассылки со всеми её атрибутами
    """
    def post(self):
        """
        file: swagger_forms/add_mailingllist.yml
        """
        data = request.form
        valid = mailingvalidate(data)
        if valid.get('data'):
            return 'не валидные данные для записи', 304
        else:
            mail = MailingList(**valid)
            sess.add(mail)
            sess.commit()
            # ищем всех клиентов, подходящих под фильтр
            clients = sess.query(Client).filter(Client.tag == valid.get('filter_client')).all()
            if datetime.now() > valid.get('when_start') and datetime.now() < valid.get('when_finish'):

                # запускаем расылку -> запрос на API
                req = {
                    'clients': clients,
                    'message': valid.get('message_text'),
                    'mailinglist_id': mail.id,
                }
                mailing(req)
            elif datetime.now() < valid.get('when_start'):
                minutes = (valid.get('when_start') - valid.get('when_finish')).total_seconds() / 60
                req = {
                    'clients': clients,
                    'message': valid.get('message_text'),
                    'mailinglist_id': mail.id,
                }
                sched.add_job(lambda: mailing(req), 'interval', minutes=minutes)
                sched.start()
            return 201


class UpdateMailingListView(Resource):
    """
    обновления атрибутов рассылки
    """
    def post(self):
        """
        file: swagger_forms/update_mailingllist.yml
        """
        data = request.form
        valid = mailing_update_validate(data)
        sess.query(MailingList).filter(MailingList.id == valid.get('id')).update(valid)
        sess.commit()
        return 201


class DeleteMailingListView(Resource):
    """
    удаления рассылки
    """
    def post(self):
        """
        file: swagger_forms/del_mailingllist.yml
        """
        data = request.form
        sess.query(MailingList).filter(MailingList.id == data.get('id')).delete()
        sess.commit()
        return 201


class TotalStatistics(Resource):
    """
    получения общей статистики по созданным рассылкам
    """
    def get(self):
        """
        file: swagger_forms/total_statistics.yml
        """
        message = sess.query(Message.mailinglist_id, Message.status).all()
        schema = MessageSchema(many=True)
        data = schema.dump(message)
        # колличество сообщений
        total_message_count = len(data)
        # колличество рассылок
        mailng = sess.query(Message.mailinglist_id, Message.status).distinct(Message.mailinglist_id).all()
        mailng_count = len(mailng)
        result = {}
        for i in data:
            if not result.get(i.get('mailinglist_id')):
                if i.get('status') == False:
                    result[i.get('mailinglist_id')] = {'False': 1}
                else:
                    result[i.get('mailinglist_id')] = {'True': 1}
            else:
                val = result.get(i.get('mailinglist_id'))
                if i.get('status') == False:
                    if val.get('False'):
                        result[i.get('mailinglist_id')]['False'] = val.get('False') + 1
                    else:
                        result[i.get('mailinglist_id')]['False'] = 1
                if i.get('status') == True:
                    if val.get('True'):
                        result[i.get('mailinglist_id')]['True'] = val.get('True') + 1
                    else:
                        result[i.get('mailinglist_id')]['True'] = 1
        return {
            'total_message_count': total_message_count,
            'mailng_count': mailng_count,
            'result': result
        }


class DetailedStatistics(Resource):
    """
    получения детальной статистики отправленных сообщений по конкретной рассылке
    """
    def get(self):
        """
        file: swagger_forms/detailed_statistics.yml
        """
        data = request.form
        messages_mailing = sess.query(Message).filter(Message.mailinglist_id == data.get('id')).all()
        result = []
        for i in messages_mailing:
            result.append({
                'number_telephone': i.client.number_telephone,
                'message': i.mailinglist.message_text,
                'date_start': str(i.mailinglist.when_start),
                'status': i.status
            })
        return result
