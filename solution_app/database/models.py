from datetime import datetime
from pytz import timezone

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean, BigInteger
from sqlalchemy.orm import relationship
from .connected import Base


UTC = timezone('UTC')


def time_now():
    return datetime.now(UTC)


class MailingList(Base):
    __tablename__ = 'mailinglist'

    id = Column(Integer, primary_key=True, autoincrement=True)
    when_start = Column(DateTime(timezone=True), default=datetime.utcnow)
    when_finish = Column(DateTime(timezone=True), default=datetime.utcnow)
    message_text = Column(String)
    filter_client = Column(String)
    message_m = relationship("Message", backref="message_m")

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
            return (
                f"<{self.__class__.__name__}("
                f"id={self.id}, "
                f"message={self.message_text}, "
                f"when_start={self.when_start}, "
                f"filter_client={self.filter_client}, "
                f")>"
            )


class Client(Base):
    __tablename__ = "client"

    id = Column(Integer, primary_key=True, autoincrement=True)
    number_telephone = Column(BigInteger)
    code = Column(Integer)
    tag = Column(String)
    timezone = Column(DateTime(timezone=True), nullable=False, default=time_now)
    message_cl = relationship("Message", backref="message_cl")

    # требуется для доступа к столбцам со значениями сервера по умолчанию
    # или значениями выражения SQL по умолчанию после сброса,
    # без запуска загрузки с истекшим сроком действия
    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
            return (
                f"<{self.__class__.__name__}("
                f"id={self.id}, "
                f"number={self.number_telephone}, "
                f")>"
            )


class Message(Base):
    __tablename__ = "message"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date_departure = Column(DateTime(timezone=True), default=datetime.utcnow)
    status = Column(Boolean, unique=False, default=True)
    mailinglist_id = Column(ForeignKey("mailinglist.id"))
    client_id = Column(ForeignKey("client.id"))

    mailinglist = relationship("MailingList", backref="meiling_m")
    client = relationship("Client", backref="client_m")

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
        return (f"<{self.__class__.__name__}, id={self.id}, status={self.status}" f")>")
