from run import ma
from .models import *


class ClientSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Client
        include_fk = True
        load_instance = True

    id = ma.auto_field()
    number_telephone = ma.auto_field()
    code = ma.auto_field()
    tag = ma.auto_field()
    timezone = ma.auto_field()
    message_cl = ma.auto_field()


class MailingListSchema(ma.SQLAlchemySchema):
    class Meta:
        model = MailingList
        include_fk = True
        load_instance = True

    id = ma.auto_field()
    when_start = ma.auto_field()
    when_finish = ma.auto_field()
    message_text = ma.auto_field()
    filter_client = ma.auto_field()
    message_m = ma.auto_field()


class MessageSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Message
        include_fk = True
        load_instance = True

    id = ma.auto_field()
    date_departure = ma.auto_field()
    status = ma.auto_field()
    mailinglist_id = ma.auto_field()
    client_id = ma.auto_field()
