from datetime import datetime
import requests
from solution_app.database.connected import sess
from solution_app.database.models import *


def clientvalidate(data: dict) -> dict:
    res = {k: v for k, v in data.items() if v is not None}
    if res.get('number_telephone'):
        res['number_telephone'] = int(res.get('number_telephone'))
    if res.get('code'):
        res['code'] = int(res.get('code'))
    if res.get('id'):
        res['id'] = int(res.get('id'))
    return res


def clien_update_tvalidate(data: dict) -> dict:
    res = {k: v for k, v in data.items() if v is not None}
    if res.get('number_telephone'):
        res[Client.number_telephone] = int(res.get('number_telephone'))
        res.pop('number_telephone')
    if res.get('code'):
        res[Client.code] = int(res.get('code'))
        res.pop('code')
    if res.get('id'):
        code = {'id': int(res.get('id'))}
        res.update(code)
    if data.get('tag'):
        res[Client.tag] = res.get('tag')
        res.pop('tag')

    return res


def mailingvalidate(data: dict) -> dict:
    when_start = datetime.strptime(data.get('when_start'), "%Y-%m-%d")
    when_finish = datetime.strptime(data.get('when_finish'), "%Y-%m-%d")
    if not data.get('filter_client'):
        return {'data': ValueError}

    else:
        return dict(when_start=when_start,
                    when_finish=when_finish,
                    filter_client=data.get('filter_client'),
                    message_text=data.get('message_text')
                    )


def mailing(data: dict):
    for i in data.get('clients'):
        # запишем в БД информацию по сообщению
        mess = Message(status=False, mailinglist_id=data.get('mailinglist_id'), client_id=i.id)
        sess.add(mess)
        sess.commit()
        token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTExNTQ2OTgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkFsZXhhbmRyX1N1In0.RMKauC4VuB5RBfEfLYj4DT9pWBNXlRHYvzPFdtF60Mg'
        response = requests.post(
            'https://probe.fbrq.cloud/v1',
            data={
                'msgId': mess.id,
                'phone': i.number_telephone,
                'text': data.get('message'),
                'Authorization': token
            }
        )
        # если ответ положительный - меняем статус в сообщениях на True
        if response.status_code == 200:
            sess.query(Message).filter(Message.id == mess.id).update({Message.status: True})

def mailing_update_validate(data: dict) -> dict:
    res = {k: v for k, v in data.items() if v is not None}
    if res.get('when_start'):
        res[MailingList.when_start] = datetime.strptime(data.get('when_start'), "%Y-%m-%d")
        res.pop('when_start')
    if res.get('when_finish'):
        res[MailingList.when_finish] = datetime.strptime(data.get('when_finish'), "%Y-%m-%d")
        res.pop('when_finish')
    if res.get('message_text'):
        res[MailingList.message_text] = data.get('message_text')
        res.pop('message_text')
    if res.get('filter_client'):
        res[MailingList.filter_client] = data.get('filter_client')
        res.pop('filter_client')

    return res