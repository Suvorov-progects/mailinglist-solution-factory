# Приложение для рассылки сообщений клиентам ФР
## СТЕК:
    * linux
    * python
    * flask
    * sqlalchemy
    * postgresql
    * swagger
    * marshmello
1. Создать БД:
```angular2html
sudo -u postgres psql
CREATE DATABASE solution;
```
при желании можно создать пользователя с правами:
```angular2html
CREATE USER test_user WITH password 'qwerty';
GRANT ALL ON DATABASE test_database TO test_user;
```

2. В корне проекта создать файл ```.env``` в котором в перменной DB указать путь к БД, например ```DB="ppostgresql+psycopg2://postgres:postgres@localhost/solution"```

3. Установить все необходимые зависимости командой ```pip install -r requirements.txt```
4. перейти в директорию приложения командом ```cd solution_app/```

5. Инициализируйте Alembic командой ```alembic init migrations```
   
6. Для проведения миграций необходимо:
- в файле ```alembic.ini``` указать путь к БД, напрмер ```sqlalchemy.url = postgresql+psycopg2://postgres:postgres@localhost/solution```
- в папке ```migration``` в файле ```env.py``` указать путь к своим моделям, например ```from app.models import *``` и изменить 
```
from database.models import *
from database.connected import Base
target_metadata = Base.metadata
```
7. Провести первую миграцию командой ```alembic revision --autogenerate -m "init"```
8. Применить миграцию командой ```alembic upgrade head"```
9. Запустите приложение командой ```python run.py```
10. С документацие можно ознакомиться по адресу ```http://localhost:5000/apidocs/#/```
11. адреса для запросов: 
```
/add_client - добавление клиента
/update_client - обновление данных по клиенту
/delete_client - удаление клиента

/add_mailing - добавление рассылки
/update_mailing - обновление данных по рассылке
/delete_mailing - удаление рассылки

/total_statistic - общая статистика по рассылкам
/detailed_statistics - детальная статитстика по конкретной рассылке
```


